### HEAD

### 1.0.0 (June 10, 2015)

* Rewrite build script to make it more customizable and flexible.
* Add a humans.txt.
* Numerous `.htaccess` improvements (including inline documentation).
* Move the alternative server configurations to the H5BP server configs repo.
* Use a protocol-relative url to reference jQuery and prevent mixed content warnings.
* Optimize the Google Analytics snippet.
* Use Eric Meyer's recent CSS reset update and the HTML5 Doctor reset.
* More robust `sub`/`sup` CSS styles.
* Add keyboard `.focusable` helper class that extends `.visuallyhidden`.
* Print styles no longer print hash or JavaScript links.
* Add a print reset for IE's proprietary filters.
* Remove IE9-specific conditional class on the `html` element.
* Remove margins from lists within `nav` elements.
* Remove YUI profiling.
