# HTML/CSS Default Templates
HTML/CSS Default Templates is a front-end boileplate for building responsive-first HTML websites.
It is  like the popular HTML5 boilerplate or initializr but with simplicity in mind.

## Features

* HTML5 ready.
* Cross-browser compatibility (Chrome, Opera, Safari, Firefox 3.6+, IE7+).
* Includes normalize via CDN (http://necolas.github.com/normalize.css/) for CSS normalizations and common bug fixes.
* Includes the latest [jQuery](http://jquery.com/) via CDN, with a local fallback.
* Includes the latest [Modernizr](http://modernizr.com/) build for feature detection.
* IE-specific classes for easier cross-browser control.
* Placeholder CSS Media Queries.
* Includes a JQUERY app quick implementation

## Folder structure and files

**Folders:**

* css: The css folder. Place all the stylesheets of the project. 
* js: The scripts folder, place there all the scripts of the project. 
* img: The images folder, place there all the images of the project
* fonts: The fonts folder, place there all the local fonts files (if any).


**Files included:**

* index.html: This is the default html file having everything you need to start a project. Edit it as required.
* css/style.css: This is the main stylesheet. Includes reset/normilizing rules for maximum cross-browser compatibility
* js/main.js: This is the main javascript file. Includes JQUERY apps implementation.


## Credits
* Author: Antonios Dimas
* Year: 2015
* Email: adonisdim1@yahoo.gr
* Bitbucket: https://bitbucket.org/adonisdim