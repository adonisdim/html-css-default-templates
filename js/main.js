/* Global settings */
var settings = {
	debug : false
};
/* Output to console if debug is on */
function echo( message ) {
	if ( settings.debug ) {
		console.log( message );
	};
};
/* IE detection flag */
jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();
/* Global help-vars */
var resizeTimeout;
(function($){
	var app = {
		'init': function(e) {
			echo( 'App init...' );
			app.fixPlaceholders();
			$(window).on('resize', app.onResize);
		},
	    /* Fix placeholders if they are not supported by the browser*/
		'fixPlaceholders' : function() {
			var input = document.createElement("input");
			if(('placeholder' in input)==false) { 
				$('[placeholder]').focus(function() {
					var i = $(this);
					if(i.val() == i.attr('placeholder')) {
						i.val('').removeClass('placeholder');
						if(i.hasClass('password')) {
							i.removeClass('password');
							this.type='password';
						}			
					}
				}).blur(function() {
					var i = $(this);	
					if(i.val() == '' || i.val() == i.attr('placeholder')) {
						if(this.type=='password') {
							i.addClass('password');
							this.type='text';
						}
						i.addClass('placeholder').val(i.attr('placeholder'));
					}
				}).blur().parents('form').submit(function() {
					$(this).find('[placeholder]').each(function() {
						var i = $(this);
						if(i.val() == i.attr('placeholder'))
							i.val('');
					})
				});
			}
		},
		'onResize' : function() {
			clearTimeout( resizeTimeout );
			resizeTimeout = setTimeout(function() {

			}, 10);
		}
	};	
	$(document).on('ready', app.init);
})(jQuery);


